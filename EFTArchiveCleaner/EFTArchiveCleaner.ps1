﻿# -----------------------------------------
# EFTArchiveCleaner.ps1
# -----------------------------------------
# Cleans out old archive files using the details 
# found within the 'settings.xml' file
# -----------------------------------------
# Author - Niall McConnell (Clever Consulting SRL)
# Release - 15/02/2019
# -----------------------------------------
# Function : Check-Files 
# Description : Check the files within the path to see if they 
# exceed the day limit. If a file exceeds the day limit and 
# the delect action is allowed, the file is removed.
#
# Parameters -
# path : The path to check
# -----------------------------------------
function Check-Files {
	Param (
          [Parameter(Mandatory=$true)] [String]$path,
          [Parameter(Mandatory=$false)] [String]$limit
		)
	# Debug output
	# Write-Host "Checking Files in $($childPath) with limit $($limit)"
	# Read each of the files in the path
	Get-ChildItem $path |foreach {
		$fullPath = "$($path)\$($_)"
		if (Test-Path "$($fullPath)" -OlderThan (Get-Date).AddDays(-$limit)) {
			if ($Settings.ScanDelete -eq $true) {
				Remove-Item -Path $fullPath
				Log-Write-Info "Limit $limit. Removed $($fullPath)"
			} else {
				Log-Write-Info "Limit $limit. Found but not removed $($fullPath)"
			}
		} 
	}	
}
function Check-Limit {
	Param (
		  [Parameter(Mandatory=$true)] [String]$subfolder,
		  [Parameter(Mandatory=$false)] [object]$custom
	)
	# Set the return day limit to the default
	$returnlimit = $Settings.Days;
	if (![string]::IsNullOrEmpty($custom)) { 
		# First check to see if the custom object is an array of custom objects
		if ($custom -is [Array]) {
			$custom |foreach {
				$foundCustom = $_
				$custommatch = $foundCustom| Where-Object { $_.subfolder -eq $subfolder}
				if (![string]::IsNullOrEmpty($custommatch)) { 
					$returnlimit = $custommatch.limit
				}
			}
		} else {
			$custommatch = $custom| Where-Object { $_.subfolder -eq $subfolder}
			if (![string]::IsNullOrEmpty($custommatch)) { 
				$returnlimit = $custommatch.limit
			} 
		}
	}
	# Default is returned if nothing is found
	$returnLimit
}
# -----------------------------------------
# Function : Check-Folder
# Description : Check the folder for the matching "Archive" 
# folder name. If an "Archive" folder is found, check the contents.
# Otherwise ignore the folder, unless the depth value
# allows the checking of its sub-folders
#
# Parameters -
# path : The path to check
# depth : The current depth of the check
# -----------------------------------------
function Check-Folder {
	Param (
         [Parameter(Mandatory=$true)] [String]$path,
		 [Parameter(Mandatory=$true)] [String]$depth,
		 [Parameter(Mandatory=$false)] [String]$subpath,
		 [Parameter(Mandatory=$false)] [object]$custom
	)
	# Debug output
	# Write-Host "Check-Folder for Path $($path)"
	if(Test-Path $path -pathType container){
		# Only process folders 
		$currentFolder = $path | split-path -leaf
		if ([string]::IsNullOrEmpty($subpath)) {
			$customFolder = "$($currentFolder)" 
		} 
		else 
		{
			$customFolder = "$($subpath)/$($currentFolder)" 
		}
		# Read the files in each of the paths
		Get-ChildItem $path |foreach {
			$childPath = "$($path)\$($_)"
			# Check if the found item has "Archive" 
			if ($_ -like "$($Settings.ScanFolder)") {
				# Check if the childPath has a matching 'custom' entry
				$dayLimit = Check-Limit $customFolder $custom
				# Debug output
				# Write-Host "Check match for childpath $($childPath) with LIMIT $($dayLimit)"
				# Write-Host "Check $($customFolder) against custom = $($custommatch.limit)"
				# If it is, check the contents
				Check-Files $childPath $daylimit
			} # Else just ignore
			# Check if the check should loop into the sub-directory
			if ($depth -gt 0) {
				# Debug output
				# Write-Host "ReChecking $($childPath)"
				Check-Folder $childPath ($depth-1) -subpath $currentFolder -custom $custom
			}
		}
	}
}
# -----------------------------------------
# Function : Check-Paths
# Description : Initiate the reading of the folders found in 
# the configured 'paths'
# -----------------------------------------
function Check-Paths
{
	$depth = $Settings.ScanDepth
	$Settings.Paths |foreach {
		# Check each of the paths
		$path = $_.folder
		Check-Folder $path $depth -custom $_.custom
	}
}

# ---------------------------------------------
# Function : Get-Setting
# Description : Returns the setting value based on the 
# predefined xml format
#
# Parameters - 
# key - setting 'key' attribute value
# Return - 
# string containing setting value found
# ---------------------------------------------
function Get-Setting
{
	Param (
          [Parameter(Mandatory=$true)] [String]$Key
	)
	$settingFile = "{0}\settings.xml" -f $PSScriptRoot 
	$xmlfile = [xml](get-content $settingFile)
    $node = $xmlfile.settings.setting  | where {$_.key -eq $Key} 
    $value = $node."#Text"
    if ([string]::IsNullOrEmpty($value)) {
        # Throw an error as the setting was not found
        $errMsg = "Failed to find setting {0} within the settings.xml file" -f $Key
        throw [System.Exception]$errMsg 
    }
    $value
}

# ---------------------------------------------
# Function : Get-Setting-Paths
# Description : Returns a string array of the path
# values from within the predefined xml format
#
# Return - 
# string array containing path values found
# ---------------------------------------------
function Get-Setting-Paths
{
	$settingFile = "{0}\settings.xml" -f $PSScriptRoot 
	$xmlfile = [xml](get-content $settingFile)
    $pathsNode = $xmlfile.settings.paths
	$paths = @()
	Foreach ($path in $pathsNode.path)
	{
		$paths += $path
	}
    $paths
}
# ---------------------------------------------
# Function : Load-Settings
# Description : Retrieve the settings from the settings.xml 
# file and load them into the global variable 'Settings'
# ---------------------------------------------
function Load-Settings
{
	try{		
		$Settings.LogFile = Get-Setting "log_file"
		$Settings.LogSize = Get-Setting "log_size"
		$Settings.Days = Get-Setting "days"
		$Settings.ScanDelete = Get-Setting "scan_delete"
    	$Settings.ScanDepth = Get-Setting "scan_depth"
    	$Settings.ScanFolder = Get-Setting "scan_folder"   
	    # Set the required test variables from the settings
		$Settings.Paths = Get-Setting-Paths
    } catch [Exception] {
        # Set the logging of the messages to false, as logging may fail
        $logError = ${false}
        throw $_   
    }
}

# ---------------------------------------------
# Function : Log-Write
# Description : Log Write to file
#
# Parameters -
# message : The message to write to log
# print : Flag to stop the write to log action
# ---------------------------------------------
function Log-Write([string]$message, [bool] $print)
{
    # Write the message to the log
    Write-Host $message
	if ($print) {
	    Add-Content $Settings.LogFile -value $message
	}
}

# ---------------------------------------------
# Function : Log-Write-Error
# Description : Write error message to the log file
#
# Parameters -
# message : The message to write to log
# print : Flag to stop the write to log action
# ---------------------------------------------
function Log-Write-Error()
{
	Param (
          [Parameter(Mandatory=$true)] [String]$message,
          [int]$print   = $true # default
	)
    $timestamp = Get-Date -Format "yyyyMMdd HH:mm:ss"
    $logMsg = "{0} ERROR {1}" -f $timestamp, $message
    Log-Write ($logMsg, $print);
}

# ---------------------------------------------
# Function : Log-Write-Error
# Description : Write informational message to the log file
#
# Parameters -
# message : The message to write to log
# print : Flag to stop the write to log action
# ---------------------------------------------
function Log-Write-Info{
	Param (
          [Parameter(Mandatory=$true)] [String]$message,
          [int]$print   = $true # default
	)
    $timestamp = Get-Date -Format "yyyyMMdd HH:mm:ss"
    $logMsg = "{0} INFO {1}" -f $timestamp, $message
    Log-Write $logMsg $print;
}

# ---------------------------------------------
# Function : Rotate_Log
# Description : Check all the logs to see if any exceed 
# the log limit. When a log exceeds the limit, rename it with a timestamp
# and start a new log
# ---------------------------------------------
function Rotate_Log {
    $threshold = $Settings.LogSize
    $filesize = [math]::Round((Get-Item $Settings.LogFile).length/1MB)
	if ($filesize -ge $threshold) { 
        Log-Write-Info "Rotate log file $($Settings.LogFile). Found to exceed $threshold MB"
		$logFileItem = Get-ChildItem $Settings.LogFile
		$datetime = Get-Date -uformat "%Y%m%d%H%M"
		# Add the timestamp to the new file name
		$newname = $logFileItem.DirectoryName +'\' + $logFileItem.BaseName + '-' + $datetime + $logFileItem.Extension
		Write-Debug $newname
		Rename-Item $Settings.LogFile $newname
		Log-Write-Info "New log created. Previous log renamed as $newname"
    }
    else{
        Log-Write-Info "Log check $($Settings.LogFile). $($filesize) < $threshold MB"
    }
}

# ---------------------------------------------
# Start of the main script
# ---------------------------------------------
Write-Host "Starting EFTArchiveCleaner"
# Create the global Configurations object
$global:Settings = "" | select Days, ScanDelete, ScanDepth, ScanFolder, LogFile, LogSize, Paths
try {
	# Step 1: Load the configurations
	Load-Settings
	# Read files from the settings and remove any files that exceed the time limit
	Check-Paths
	# Check the logs and rotate when required 
	Rotate_Log

} catch [Exception] {
	# Set the logging of the messages to false, as logging may fail
    $logError = ${false}
    throw $_   
  } 